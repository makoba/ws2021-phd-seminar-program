\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\bibliography{references.bib}

\title{PhD-seminar on Shimura varieties}
\author{Aryaman Patel and Manuel Hoff}
\date{winter term 2021/2022}

\newcommand{\QQ}{\mathbf{Q}}
\newcommand{\Af}{\mathbf{A}_f}
\newcommand{\RR}{\mathbf{R}}
\newcommand{\CC}{\mathbf{C}}
\newcommand{\deltorus}{\mathbf{S}}
\newcommand{\Sh}{\mathrm{Sh}}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Lie}{Lie}
\DeclareMathOperator{\Hol}{Hol}

\begin{document}
    
    \maketitle

    \section*{Introduction}

    A \emph{Shimura datum} (as defined by Deligne in \cite{deligne-travdeshimura}) is a pair $(G, X)$ consisting of a reductive $\QQ$-algebraic group $G$ and a $G(\RR)$-conjugacy class of maps of $\RR$-algebraic groups $\deltorus \to G_{\RR}$ that satisfy a bunch of properties.
    Here $\deltorus$ is a certain $\RR$-algebraic group that is sometimes called the \emph{Deligne torus}.
    Given such a Shimura datum $(G, X)$ one can define an associated number field $E$ (called the \emph{reflex field}) and a system of algebraic varieties $(\Sh_K(G, X))_K$ over $E$ with finite étale transition maps that is indexed over (small enough) compact open subgroups $K \subseteq G(\Af)$.
    This system of algebraic varieties is what is called the \emph{Shimura variety} associated to the Shimura datum.

    The construction of the Shimura variety is done roughly in three steps:

    \begin{enumerate}
        \item
        Equip $X$ with the structure of a complex manifold.

        \item
        Define the topological space
        \[
            \Sh_K(G, X)(\CC) \coloneqq G(\QQ) \backslash (X \times G(\Af)) / K
        \]
        and show that this is a finite disjoint union of topological spaces of the form $\Gamma \backslash X^+$ where $X^+$ is a connected component of $X$ and $\Gamma \subseteq G(\QQ)$ is a certain subgroup that acts properly discontinuously on $X^+$.
        Thus we can equip also $\Sh_K(G, X)(\CC)$ with the structure of a complex manifold.
        It turns out that this complex manifold comes from a smooth complex variety $\Sh_K(G, X)$ that is uniquely determined by that property.

        \item
        Define the reflex field $E$ and a \emph{canonical model} of the smooth complex varieties $\Sh_K(G, X)$ over $E$.
    \end{enumerate}

    One remarkable feature of Shimura that they carry two kinds of symmetries.
    On one hand we have a (continuous) action of the group $G(\Af)$ on the system $(\Sh_K(G, X))_K$ and on the other hand the étale cohomology of the Shimura variety has an action of the absolute Galois group $G_E$ of $E$.

    The goal of this seminar is to understand (at least a little of) the definition of a Shimura datum and the construction of the associated Shimura variety.
    Along the way we will also learn something about Abelian varieties.
    These are higher-dimensional analoga of elliptic curves and are closely related to the Siegel modular varieties that will be our main/only examples of Shimura varieties.

    \section*{Talks}
    
    \begin{enumerate}[label = \textbf{Talk \arabic*:}]
        \setcounter{enumi}{-1} 
        
        \item
        \textbf{Introduction (on 11.10.\ by Aryaman and Manuel).}

        \item
        \textbf{Algebraic Groups and Lie Groups (on 25.10.\ by Paulina).}

        \noindent
        For algebraic groups we work over a base field of characteristic $0$.

        \begin{itemize}
            \item
            (Affine) algebraic groups and basic properties (\cite[Chapter 1, 5]{milne-alggrps-book}).

            \item
            The Lie algebra of an algebraic group and the adjoint representation (\cite[Chapter 10]{milne-alggrps-book}).

            \item
            Groups of multiplicative type (in particular tori), unipotent groups, solvable groups (\cite[Chapter 6, 12, 14, 16]{milne-alggrps-book}).

            \item
            The (unipotent) radical of an algebraic group, reductive groups, semisimple groups, adjoint groups (\cite[Chapter 6.h, 19]{milne-alggrps-book}, the definition of an adjoint group is hidden on page 373).

            \item
            Lie groups and the Lie algebra of a Lie group as well as the adjoint representation of a Lie group (\cite[Chapter 4]{tu-manifolds}).
            Remark that the real points of an $\RR$-algebraic group naturally form a Lie group (\cite[page 158]{milne-algliegrps}).
            Also remark that the being a Lie group is a \emph{property} of a topological group.
        \end{itemize}

        \item
        \textbf{Hermitian Symmetric Domains (on 08.11.\ by Ravjot).}

        \begin{itemize}
            \item
            Hermitian symmetric domains (\cite[page 5-10]{milne-svi}).

            \item
            Automorphism groups of hermitian symmetric domains (\cite[page 12-13]{milne-svi}).

            \item
            The $\RR$-algebraic group $U_1$.
            I think there is no concrete definition in the reference.

            \item
            The map $u_p \colon U_1 \to \Hol(D)$ (\cite[page 13-14]{milne-svi}).

            \item
            Cartan involutions (\cite[page 15]{milne-svi}).

            \item
            Classification of Hermitian symmetric domains (\cite[page 17-19]{milne-svi}).
        \end{itemize}
        
        \item
        \textbf{Locally Symmetric Varieties (on 15.11.\ by Alessandro).}

        \begin{itemize}
            \item
            Quotients of Hermitian symmetric domains by discrete groups (\cite[page 32]{milne-svi}).

            \item
            Arithmetic subgroups (\cite[page 33-35]{milne-svi}).
            Note that there exists a notion of arithmetic subgroup both for $\QQ$-algebraic groups and Lie groups.

            \item
            Smooth complex varieties (i.e. smooth and quasi-compact $\CC$-schemes) and complex manifolds (\cite[36-38]{milne-svi}).

            \item
            The theorems of Baily-Borel and Borel (\cite[page 38-40]{milne-svi}).
        \end{itemize}
        \item
        \textbf{Connected Shimura Varieties (on 22.11.\ by Robin).}

        \begin{itemize}
            \item
            Congruence subgroups and connection to adèles (\cite[page 42-43]{milne-svi}).
            I think it is easier to topologize $V(\Af)$, for an affine $\QQ$-scheme $V$ of finite type, by using a closed immersion of $V$ into some affine space.

            \item
            Connected Shimura data (\cite[page 44-45]{milne-svi}).

            \item
            Connected Shimura varieties (\cite[page 46-47]{milne-svi}).

            \item
            Adèlic description of connected Shimura varieties (\cite[page 48-49]{milne-svi}).
            Skip 4.19 and 4.20.

            \item
            The $\RR$-algebraic group $\deltorus$ and another definition of a connected Shimura datum (\cite[page 50-51]{milne-svi}).
            I think there is no concrete definition in the reference.
        \end{itemize}

        \item
        \textbf{Shimura Varieties 1 (on 29.11.\ by Virginie).}

        \begin{itemize}
            \item
            Shimura data and Shimura varieties (\cite[page 52-58]{milne-svi}).
        \end{itemize}

        \item
        \textbf{Shimura Varieties 2 (on 06.12.\ by Jonas).}

        \begin{itemize}
            \item
            Maps of Shimura varieties (\cite[page 58-59]{milne-svi}).
            
            \item
            Simply connected semisimple algebraic groups.

            \item
            The structure of a Shimura variety (\cite[page 59-63]{milne-svi}).
        \end{itemize}

        \item
        \textbf{Abelian Schemes (on 13.12.\ by Dario).}
        
        \noindent
        Always assume that the base scheme is of characteristic $0$.
        References in the case where the base is a field are \cite{mumford} and \cite{milne-abvars}.

        \begin{itemize}
            \item
            Abelian schemes.

            \item
            The $n$-torsion $A[n]$ and the integral and rational Tate modules $T_f A$ and $V_f A$ of an Abelian scheme.

            \item
            The dual Abelian scheme and the Weil pairing.

            \item
            Isogenies.

            \item
            Mention that one can recover an Abelian scheme from the associated Abelian scheme up to isogeny together with its integral (opposed to rational) Tate module. 

            \item
            Polarisations.
        \end{itemize}

        \item
        \textbf{Hodge Structures and Abelian Schemes over $\CC$ (on 20.12.\ by Anneloes).}

        \begin{itemize}
            \item
            (Variations of) Hodge structures (\cite[Chapter 2]{milne-svi}).

            \item
            Connection between Shimura data and Hodge structures (\cite[Proposition 5.9]{milne-svi}).

            \item
            Abelian schemes over $\CC$ and complex tori (\cite[page 71-73]{milne-svi}).

            \item
            Abelian schemes over smooth algebraic schemes over $\CC$ (\cite[page 121-122]{milne-svi}).
        \end{itemize}

        \item
        \textbf{Siegel Modular Variety 1 (on 10.01.\ by Ludvig).}

        \begin{itemize}            
            \item
            The Siegel Shimura datum associated to a symplectic space over $\QQ$ (\cite[page 68-69]{milne-svi}).

            \item
            Level structures of abelian schemes (I would like to do this again over general base schemes of characteristic $0$).

            \item
            Moduli description of the Siegel Shimura variety over $\CC$ (\cite[Theorem 6.11]{milne-svi} but I think it is also possible to have a statement about families).
            Discuss in particular the special case where the level structure is $K(N)$ and explain how the set of connected components looks like here.
        \end{itemize}

        \item
        \textbf{Canonical Models of Shimura Varieties 1 (on 17.01.\ by Marc).}

        \begin{itemize}
            \item
            Review of class field theory and the Artin reciprocity map (\cite[page 106-107]{milne-svi}).
            
            \item
            The reflex field of a Shimura datum (\cite[page 111-112]{milne-svi}).

            \item
            The canonical model of a Shimura variety associated to a Shimura datum of the form $(T, \{ h \})$ where $T/\QQ$ is a torus (\cite[page 114-115]{milne-svi}).
        \end{itemize}

        \item
        \textbf{Canonical Models of Shimura Varieties 2 (on 24.01.\ by Chirantan).}

        \begin{itemize}
            \item
            Special points on Shimura varieties (\cite[page 113]{milne-svi}).

            \item
            The notion of a canonical model of a Shimura variety (\cite[page 114-115]{milne-svi}).

            \item
            Uniqueness of canonical models (\cite[Chapter 13]{milne-svi}).
        \end{itemize}

        \item
        \textbf{Siegel Modular Variety 2 (on 31.01.\ by Luca).}

        \begin{itemize}
            \item
            CM Abelian varieties and the main theorem of complex multiplication.

            \item
            Special points on the Siegel Modular variety.

            \item
            Existence of the canonical model for the Siegel modular variety and moduli description.
        \end{itemize}

    \end{enumerate}

    \printbibliography
\end{document}