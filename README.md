# ws2021-phd-seminar-program

This is the seminar program for the (unofficial) PhD seminar of the ESAGA in the winter term 2021/2022 about 'Shimura varieties' that is organized by Aryaman Patel and myself.
A compiled version can be found [here](https://makoba.gitlab.io/ws2021-phd-seminar-program/phd-seminar-program.pdf).